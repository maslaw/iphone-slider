function load() {

    var $preloader = $('#page-preloader'),
        $spinner = $preloader.find('.spinner');

    $spinner.fadeOut(1000);


    jQuery(document).ready(function($) {
        //object save info about screens and events which occur on screen
        var animationQueue = [
            //first
            {
                id: 'start',
                screen: 'start',
                delay: 1500
            },
            {
                id: 'start-exit',
                screen: 'start',
                delay: 500,
                do: function(animation) {
                    da_toch();
                }
            },
            //second
            {
                id: 'second-screen-show',
                screen: 'second',
                delay: 1500
            },
            {
                id: 'second-screen-click',
                screen: 'second',
                delay: 500,
                do: function(animation) {
                    da_toch();
                }
            },
            //third
            {
                id: 'third-screen-show',
                screen: 'third',
                delay: 1500
            },
            {
                id: 'third-screen-click',
                screen: 'third',
                delay: 500,
                do: function(animation) {
                    da_toch();
                }
            },
            //fourth
            {
                id: 'fourth-screen-show',
                screen: 'fourth',
                delay: 1500
            },
            {
                id: 'fourth-screen-click',
                screen: 'fourth',
                delay: 500,
                do: function(animation) {
                    da_toch();
                }
            },
            //fifth
            {
                id: 'fifth-screen-show',
                screen: 'fifth',
                delay: 1500
            },
            {
                id: 'fifth-screen-click',
                screen: 'fifth',
                delay: 500,
                do: function(animation) {
                    da_toch();
                }
            },
            //sixth
            {
                id: 'sixth-screen-show',
                screen: 'sixth',
                delay: 1500
            },
            {
                id: 'sixth-screen-click',
                screen: 'sixth',
                delay: 500,
                do: function(animation) {
                    da_toch();
                }
            },
            //seventh
            {
                id: 'seventh-screen-show',
                screen: 'seventh',
                delay: 1500
            },
            {
                id: 'seventh-screen-click',
                screen: 'seventh',
                delay: 500,
                do: function(animation) {
                    da_toch();
                }
            },
            //eighth
            {
                id: 'eighth-screen-show',
                screen: 'eighth',
                delay: 1500
            },
            {
                id: 'eighth-screen-click',
                screen: 'eighth',
                delay: 500,
                do: function(animation) {
                    da_toch();
                }
            },
            //ninth
            {
                id: 'ninth-screen-show',
                screen: 'ninth',
                delay: 1500
            },
            {
                id: 'ninth-screen-click',
                screen: 'ninth',
                delay: 500,
                do: function(animation) {
                    da_toch();
                }
            }
        ];

        //function touching screen
        function da_toch() {
            setTimeout(function(){
                $('.da_toch').removeClass('active');
            }, 400);
            $('.da_toch').addClass('active');
        }


        var animationIndex = 0; //index of screen
        var curAnimation = undefined;
        var prevAnimation = undefined;
        //console.log(animationQueue);

        var timerId = undefined;
 
        var buttonAnimation = $('.button-animation'),
            stopButton = $('.button-stop');

        $(stopButton).click(function () {
            stop();
        });

        $(buttonAnimation).click(function (e) {
            var queue = false, curSlide = $(e.target).data('id');

            animationIndex = curSlide;

            console.log('target ' + $(e.target).data('id'));


            stop();

            $('.da_screen').removeClass('da_screen_deactive da_screen_active');

            runAnimation(curSlide, e);
        });





        function runAnimation( curSlide , e ) {

            curAnimation = animationQueue[animationIndex]; //cut object(screen) from array

            if (curAnimation !== undefined) {

                //condition for changing of screens when exist previous Screen

                if (prevAnimation !== undefined || ( animationIndex !== curSlide && e )){
                    $('#demo-app-animation').removeClass('da-' + prevAnimation.id);
                    if (prevAnimation.screen !== curAnimation.screen) {

                        //delete current screen for next screen
                        $('.da_screen_active').addClass('da_screen_deactive');
                        //$('.da_screen_active').removeClass('da_screen_active');
                    }
                }

                //put new screen
                $('#demo-app-animation').addClass('da-' + curAnimation.id);
                //console.log("AAAAAAAAAAAAAA" + curAnimation.id);
                $('.da_screen_' + curAnimation.screen).addClass('da_screen_active');


                //execute property do of object(screen) touch or like function
                if (curAnimation.do !== undefined) {
                    //curAnimation.do(curAnimation);
                    curAnimation.do();
                }

                //run new animation(screen), has two parameters: (runAnimation, delay)
                start();

                //increase index of screen (next screen)
                animationIndex ++;
                console.log("animationIndex: " + animationIndex);
                //save current index of screen
                prevAnimation = curAnimation;
            }

            //******
            //if last screen(animation) have executed reset animationIndex and start new queue of animation
            if (animationIndex >= animationQueue.length) {

                animationIndex = 0;
                var last_animation_id = prevAnimation.id;
                prevAnimation = undefined;
                stop();

                $('.da_post_toch').removeClass('active');
            }
        }

        function stop() {
            clearTimeout(timerId);
        }

        //run function everytime after executing current screen(current object from array)
        function start() {
            timerId = setTimeout(runAnimation, ( curAnimation ? curAnimation.delay : 1 ) );
        }

        start();

    });
}

window.onload = load;
