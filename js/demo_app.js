function load() {

	var $preloader = $('#page-preloader'),
	$spinner = $preloader.find('.spinner');
	$spinner.fadeOut(1000);
	/*$spinner.animate({
		opacity: 1
	}, 2000, function () {
		$spinner.css("opacity", "0");
		alert("HELLO");
	});*/


	jQuery(document).ready(function($) {
		//object save info about screens and events which occur on screen
		var animationQueue = [
			//first
			{
				id: 'start',
				screen: 'start',
				delay: 1500
			},
			{
				id: 'start-exit',
				screen: 'start',
				delay: 500,
				do: function(animation) {
					da_toch();
				}
			},
			//second
			{
				id: 'second-screen-show',
				screen: 'second',
				delay: 1500
			},
			{
				id: 'second-screen-click',
				screen: 'second',
				delay: 500,
				do: function(animation) {
					da_toch();
				}
			},
			//third
			{
				id: 'third-screen-show',
				screen: 'third',
				delay: 1500
			},
			{
				id: 'third-screen-click',
				screen: 'third',
				delay: 500,
				do: function(animation) {
					da_toch();
				}
			},
			//fourth
			{
				id: 'fourth-screen-show',
				screen: 'fourth',
				delay: 1500
			},
			{
				id: 'fourth-screen-click',
				screen: 'fourth',
				delay: 500,
				do: function(animation) {
					da_toch();
				}
			},
			//fifth
			{
				id: 'fifth-screen-show',
				screen: 'fifth',
				delay: 1500
			},
			{
				id: 'fifth-screen-click',
				screen: 'fifth',
				delay: 500,
				do: function(animation) {
					da_toch();
				}
			},
			//sixth
			{
				id: 'sixth-screen-show',
				screen: 'sixth',
				delay: 1500
			},
			{
				id: 'sixth-screen-click',
				screen: 'sixth',
				delay: 500,
				do: function(animation) {
					da_toch();
				}
			},
			//seventh
			{
				id: 'seventh-screen-show',
				screen: 'seventh',
				delay: 1500
			},
			{
				id: 'seventh-screen-click',
				screen: 'seventh',
				delay: 500,
				do: function(animation) {
					da_toch();
				}
			},
			//eighth
			{
				id: 'eighth-screen-show',
				screen: 'eighth',
				delay: 1500
			},
			{
				id: 'eighth-screen-click',
				screen: 'eighth',
				delay: 500,
				do: function(animation) {
					da_toch();
				}
			},
			//ninth
			{
				id: 'ninth-screen-show',
				screen: 'ninth',
				delay: 1500
			},
			{
				id: 'ninth-screen-click',
				screen: 'ninth',
				delay: 500,
				do: function(animation) {
					da_toch();
				}
			}
			/*{
				id: 'news-like-2',
				screen: 'news',
				delay: 2000,
				do: function(animation) {
					da_toch();
					da_like(2, 'news', 1);
				}
			},
			{
				id: 'news-add',
				screen: 'news',
				delay: 700,
				do: function(animation) {
					da_toch();
				}
			},
			{
				id: 'my-list-show',
				screen: 'list',
				delay: 800
			},
			{
				id: 'my-list-add',
				screen: 'list',
				delay: 1500,
				do: function(animation) {
					da_toch();
				}
			},
			{
				id: 'my-list-like',
				screen: 'list',
				delay: 5000,
				do: function(animation) {
					da_like(null, 'list', 27);
				}
			}*/
		];

		//function touching screen
		function da_toch() {
			setTimeout(function(){
				$('.da_toch').removeClass('active');
			}, 400);
			$('.da_toch').addClass('active');
		}

		/*function delayTyping() {
			setTimeout(function () {
				alert("Wait");
			}, 1000);
		}*/

		//function occurs when tap like
		/*function da_like(order, el, count) {
			var likeTimerId,
				likes_el;

			//condition when order of touch exist
			if( order != undefined ) {
				likes_el = $('.da_screen_' + el + ' .da_post_toch_' + order );
				//console.log("TEST:" + '.da_screen_' + el + ' .da_post_toch_' + order );
			}
			//when order of touch doesn't exist
			else {
				likes_el = $('.da_screen_' + el + ' .da_post_toch' );
				//console.log("TEST-ELSE:" + '.da_screen_' + el);
			}
			var likes = likes_el.text();
			//console.log("LikeS" + likes);
			var i = 1;

			//timer for likes has two conditions (increase one and many likes)
			setTimeout(function(){
				var ims = count == 1 ? 1 : 600;
				var imsCoef = 0.62;

				function lekeTimerCallback() {
					likes ++;
					i ++;
					likes_el.html( likes );
					likes_el.addClass('active');

					//condition is true when need increase not one likes (set speed of changing likes)
					if (i < count) {
						ims *= imsCoef;

						if (ims < 50) imsCoef = 1.1;
						likeTimerId = setTimeout(lekeTimerCallback, ims);
					}
				}

				//run this timer when i >= count (need less pause, for one like)
				likeTimerId = setTimeout(lekeTimerCallback, ims);
			}, 200);
			$('.da_screen_list .da_post_toch' ).html('0');
		}*/

		var animationIndex = 0; //index of screen
		var curAnimation = undefined;
		var prevAnimation = undefined;
		//console.log(animationQueue);

		var timerId = undefined;

		var buttonAnimation = $('.button-animation'),
			stopButton = $('.button-stop');

		$(stopButton).click(function () {
			console.log("STOP");
			stop();
		});

		$(buttonAnimation).click(function (e) {
			var queue = false,
				curSlide;
			//stop();
			//start();


			stop();

			curSlide = $(e.target).data('id');
			console.log('Im' + curSlide);

			if(curSlide == 0){
				//console.log("RUUUUNNNN");
				start();
				//runAnimation();
			}

			$('.da_screen_active').removeClass('da_screen_deactive').removeClass('da_screen_active');

			runAnimation(curSlide, e);
			/*if (prevAnimation !== undefined) {

			 //delete current screen for next screen
			 $('#demo-app-animation').removeClass('da-' + prevAnimation.id);
			 if (prevAnimation.screen !== curAnimation.screen) {
			 $('.da_screen_active').addClass('da_screen_deactive');
			 //$('.da_screen_active').removeClass('da_screen_active');
			 }
			 }

			 //put new screen
			 $('#demo-app-animation').addClass('da-' + curSlide);
			 console.log(curSlide);
			 console.log(curAnimation.id);
			 $('.da_screen_' + curAnimation.screen).addClass('da_screen_active');*/
			/*if(curSlide == animationIndex){
			 alert("AAAAAAAAA");
			 $('#demo-app-animation').addClass('da-' + curAnimation.id);
			 $('.da_screen_' + curAnimation.screen).addClass('da_screen_active');
			 //start();

			 }*/
		});





		function runAnimation(curSlide , e) {
			if (curSlide) {
				//animationIndex = animationIndex + curSlide;
				//curAnimation = animationQueue[curSlide];
				console.log('11111111111111previous index' + prevAnimation);
				animationIndex = curSlide;

				/*$('.da_screen_active').addClass('da_screen_deactive');
				$('#demo-app-animation').addClass('da-' + curAnimation.id);
				$('.da_screen_' + curAnimation.screen).addClass('da_screen_active');*/
			}
			/*else if(curSlide < animationIndex){
				console.log("ELSE IF: ");
				console.log("curAnimationID: " + curAnimation.id);
				console.log("curSlide: " + curSlide);
				console.log("animationIndex in ELSE IF: " + animationIndex);

				stop();
				animationIndex = curSlide;

				console.log("animationIndex after minus in ELSE IF: " + animationIndex);

				curAnimation = animationQueue[curSlide];
			}*/

			curAnimation = animationQueue[animationIndex]; //cut object(screen) from array
			console.log('runAnimationID: ' + curAnimation.id); //****


			if (curAnimation !== undefined) {

				//condition for changing of screens when exist previous Screen

				if (prevAnimation !== undefined || ( animationIndex !== curSlide && e )){
					$('#demo-app-animation').removeClass('da-' + prevAnimation.id);
					if (prevAnimation.screen !== curAnimation.screen) {

						//delete current screen for next screen
						$('.da_screen_active').addClass('da_screen_deactive');
						//$('.da_screen_active').removeClass('da_screen_active');
					}
				}

				//put new screen
				$('#demo-app-animation').addClass('da-' + curAnimation.id);
				//console.log("AAAAAAAAAAAAAA" + curAnimation.id);
				$('.da_screen_' + curAnimation.screen).addClass('da_screen_active');


				//execute property do of object(screen) touch or like function
				if (curAnimation.do !== undefined) {
					//curAnimation.do(curAnimation);
					curAnimation.do();
				}

				//run new animation(screen), has two parameters: (runAnimation, delay)
				start();

				//increase index of screen (next screen)
				animationIndex ++;
				console.log("animationIndex: " + animationIndex);
				//save current index of screen
				prevAnimation = curAnimation;
			}

			/*if(animationIndex == 6 || animationIndex == 12 || animationIndex == 18){
				var prevButton = $('.button-prev'),
					nextButton = $('.button-next');

				stop();

				if(animationIndex == 6){
					if(nextButton){
						$(nextButton).click(function () {
							start();
						});
					}
					if(prevButton){
						$(prevButton).click(function () {
							console.log(curAnimation.screenNumber - 6);
						});
					}

				}
			}*/

			//******
			//if last screen(animation) have executed reset animationIndex and start new queue of animation
			if (animationIndex >= animationQueue.length) {
				//console.log("animationIndex: " + animationIndex + " animationQueue: " + animationQueue);

				animationIndex = 0;
				var last_animation_id = prevAnimation.id;
				prevAnimation = undefined;
				stop();

				//move all screens right except null screen
				//$('.da_screen_deactive:not(:eq(0))').removeClass('da_screen_deactive');
				//console.log('Reset Step 1');

				/*setTimeout(function(){
				    console.log('Reset Step 2');

					//remove class da_screen_deactive and da_screen_active for default stage
					$('.da_screen_deactive').removeClass('da_screen_deactive');
					$('.da_screen_active').removeClass('da_screen_active');
					//move first screen(animation) to start for new circle of animation
					$('.da_screen_start').addClass('da_screen_active');

					//clear bar from start animation (from first and second screen)
					$('#demo-app-animation').removeClass('da-with-bar');
                    $('#demo-app-animation').removeClass('da-' + last_animation_id);

					$('.da_like').removeClass('active');
					//runAnimation();
				}, 5000);*/

				$('.da_post_toch').removeClass('active');

			}
		}

		function stop() {
			clearTimeout(timerId);
		}

		//run function everytime after executing current screen(current object from array)
		function start() {
			timerId = setTimeout(runAnimation, curAnimation.delay);
		}

		/*$('#da-animation-start').click(start);
		$('#da-animation-stop').click(stop);*/

		runAnimation();

	});
}

window.onload = load;
